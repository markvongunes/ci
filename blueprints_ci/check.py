#!/usr/bin/env python3

import sys
sys.path.append(".")

from blueprints_ci import (  # noqa
    logger,
    Settings,
    fetch_tests_from_squad,
)


#
#   Required environment variables
#
required_vars = [
    # Details to send results to SQUAD
    "SQUAD_HOST",
    "SQUAD_TOKEN",
    "SQUAD_GROUP",
    "SQUAD_PROJECT",

    # Results coming from trigger_tests stage
    "SQUAD_JOB_ID",
]


#
#   Check test implementation
#
def check(settings):
    """
        1. Fetch test results for a given SQUAD job id
    """

    jobs = settings.SQUAD_JOB_ID.split(',')
    results = set()
    for job_id in jobs:
        settings.SQUAD_JOB_ID = job_id
        logger.info(f"Fetching results from {settings.SQUAD_HOST}/api/testjobs/{settings.SQUAD_JOB_ID}")
        tests = fetch_tests_from_squad(settings)
        if tests is None:
            results.add("fail")
            continue

        right_padding = 50
        for test in tests:
            logger.info(f"{test.name:<{right_padding}}: {test.status}")
            results.add(test.status)

    return "fail" not in results


def main():
    settings = Settings(extra=required_vars)
    if settings.missing:
        return False
    return check(settings)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
