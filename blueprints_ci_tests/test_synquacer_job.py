import os
import subprocess

from unittest import TestCase
from unittest.mock import Mock
from blueprints_ci import (
    generate_lava_job_definition,
    send_testjob_request_to_squad,
    resolve_os_or_firmware,
)


class TestSynquacerJob(TestCase):
    """
        This test makes sure that the Synquacer job definition is being
        generated as it should, then sends it to a LAVA instance
        to make sure it actually works.
    """

    def get_git_rev_short_hash(self):
        return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()

    def get_git_hash(self):
        ci_git_hash = os.getenv("CI_COMMIT_SHORT_SHA")
        return ci_git_hash if ci_git_hash else self.get_git_rev_short_hash()

    def sample_settings(self):
        return Mock(
            SQUAD_GROUP="blueprints",
            SQUAD_PROJECT="ci",
            SQUAD_HOST=os.getenv("SQUAD_HOST"),
            NIGHTLYBUILDS_URL="https://gitlab.com/Linaro/blueprints/nightly-builds",
            LAVA_DEVICE="synquacer",
            CI_COMMIT_SHORT_SHA=self.get_git_hash(),
            SQUAD_BUILD=self.get_git_hash(),
            IMAGES_DIR="images",
        )

    def test_basics(self):
        settings = self.sample_settings()
        template = "secure-boot-enabled.yaml.jinja2"
        context = {
            "os_url": resolve_os_or_firmware(settings, "build-meta-ewaol-machine-ledge-security-qemuarm64:ewaol-baremetal-image-ledge-secure-qemuarm64.rootfs.wic.gz"),
            "firmware_url": resolve_os_or_firmware(settings, "build-meta-ts-synquacer:ts-firmware-synquacer.rootfs.tar.gz")
        }

        self.assertIsNotNone(context["os_url"])
        self.assertIsNotNone(context["firmware_url"])

        definition = generate_lava_job_definition(settings, template, context=context)
        self.assertIsNotNone(definition)

        sent = send_testjob_request_to_squad(settings, definition)
        self.assertTrue(sent)
