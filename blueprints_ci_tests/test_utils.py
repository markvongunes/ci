import glob
import os
from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci import (
    generate_tuxsuite_plan,
    resolve_os_or_firmware,
    submit_to_tuxsuite,
    find_next_stage_job_id,
    generate_lava_job_definition,
)


url = "http://some-repo"


def expected_plan():
    return {
        "jobs": [
            {
                "bake": {
                    "sources": {
                        "kas": {
                            "url": url,
                            "yaml": "kas.yml",
                        }
                    }
                }
            }
        ],
        "version": 1,
        "description": "",
        "name": None,
    }


class TestUtils(TestCase):

    def sample_settings(self):
        return Mock(
            IMAGES_DIR="images",
            NIGHTLYBUILDS_URL="http://nightly-builds-url.example",
            BLUEPRINTSCI_URL="http://blueprints-ci-url.example",
        )

    def test_generate_tuxsuite_plan(self):
        # Test with a single yml
        plan = generate_tuxsuite_plan(url, "kas.yml")
        single_kas = expected_plan()
        self.assertEqual(single_kas, plan)

        # Test with many yml
        many_yml = expected_plan()
        many_yml["jobs"][0]["bake"]["sources"]["kas"]["yaml"] = "kas.yml:build.yml"
        plan = generate_tuxsuite_plan(url, ["kas.yml", "build.yml"])
        self.assertEqual(many_yml, plan)

        # Test with ref
        ref = "1.0"
        ref_plan = expected_plan()
        ref_plan["jobs"][0]["bake"]["sources"]["kas"]["ref"] = f"refs/tags/{ref}"
        plan = generate_tuxsuite_plan(url, "kas.yml", ref=ref)
        self.assertEqual(ref_plan, plan)

        # Test with branch
        branch = "kirkstone"
        branch_plan = expected_plan()
        branch_plan["jobs"][0]["bake"]["sources"]["kas"]["branch"] = branch
        plan = generate_tuxsuite_plan(url, "kas.yml", branch=branch)
        self.assertEqual(branch_plan, plan)

    @patch('blueprints_ci.build.sp.Popen')
    def test_submit_to_tuxsuite(self, popen):
        proc = Mock()
        proc.wait.return_value = None
        proc.returncode = 0
        popen.return_value = proc

        settings = Mock(CI_JOB_NAME="sample-job", CI_JOB_ID="123")

        plan = generate_tuxsuite_plan("http://some-repo.com", "kas.yml")
        result, filename = submit_to_tuxsuite(settings, plan)

        popen.assert_called_with(['tuxsuite', 'plan', '--json', '--json-out', 'result-sample-job-123.json', 'plan-sample-job-123.yml'])
        proc.wait.assert_called()
        self.assertTrue(result)
        self.assertEqual("result-sample-job-123.json", filename)

    @patch("blueprints_ci.requests.get")
    def test_resolve_os_or_firmware(self, mock_get):
        settings = self.sample_settings()
        job_name = "sample-job-name"
        image_path = "sample-image"
        expected_url = f"{settings.NIGHTLYBUILDS_URL}/-/jobs/123/artifacts/raw/{settings.IMAGES_DIR}/{image_path}"

        response = Mock()
        response.status_code = 404
        response.headers = {"Location": expected_url}
        mock_get.return_value = response

        # Test with a plain url
        url = "http://some-url"
        expected = resolve_os_or_firmware(settings, url)
        self.assertEqual(expected, url)

        # Resolve an actual thing
        expected = resolve_os_or_firmware(settings, f"{job_name}:{image_path}")

        self.assertIsNone(expected)

        response.status_code = 302
        expected = resolve_os_or_firmware(settings, f"{job_name}:{image_path}")
        self.assertEqual(expected_url, expected)

    @patch("blueprints_ci.requests.get")
    def test_find_next_stage_job_id(self, mock_get):
        settings = self.sample_settings()
        settings.CI_JOB_NAME = 'job-name'
        settings.CI_API_V4_URL = 'https://gitlab.com/api/v4'
        settings.CI_PROJECT_ID = '1'
        settings.CI_PIPELINE_ID = '1'

        response = Mock()
        response.status_code = 200
        response.json = lambda: [{"name": "check-job-name", "id": "123"}]
        mock_get.return_value = response

        expected = find_next_stage_job_id(settings)
        self.assertEqual(expected, "123")

    def test_generate_lava_job_definition(self):
        # Test generating all possible job definitions
        devices = glob.glob("blueprints_ci/lava/templates/devices/*.yaml.jinja2")
        tests = [os.path.basename(p) for p in glob.glob("blueprints_ci/lava/templates/tests/*.yaml.jinja2")]
        settings = self.sample_settings()
        ci_job_name = "sample-test"
        settings.CI_JOB_NAME = ci_job_name
        context = {
            "os_url": "http://os.example",
            "firmware_url": "http://firmware.example",
        }
        for device_filename in devices:
            settings.LAVA_DEVICE = os.path.basename(device_filename).replace("_base.yaml.jinja2", "")

            for template in tests:
                definition = generate_lava_job_definition(settings, template, context=context)
                self.assertIsNotNone(definition)
                self.assertIn(ci_job_name, definition)

                # Make sure values for os_url and firmware_url are present in definition
                # TODO: for now, synquacer jobs do not use firmware_url, so skip checking there
                self.assertIn(context["os_url"], definition)
                if "synquacer" not in device_filename:
                    self.assertIn(context["firmware_url"], definition)
