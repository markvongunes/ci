from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.test import test

import pathlib


class TestTest(TestCase):

    def sample_settings(self):
        return Mock(
            CI_JOB_NAME="sample-job",
            CI_JOB_ID="123",
            CI_PROJECT_DIR=".",
            CI_PROJECT_PATH="this/project",
            CI_PROJECT_URL="http://this.project",
            LAVA_DEVICE="qemu",
            SQUAD_HOST="http://squad.example",
            SQUAD_TOKEN="123",
            SQUAD_GROUP="squad_group",
            SQUAD_PROJECT="squad_project",
            SQUAD_BUILD="123abc",
            IMAGES_DIR="images",
        )

    @patch("blueprints_ci.test.register_callback_in_squad")
    @patch("blueprints_ci.test.send_testjob_request_to_squad")
    def test_os_test(self, mock_send_testjob_request_to_squad, mock_register_callback_in_squad):
        mock_send_testjob_request_to_squad.return_value = (True, "1")
        mock_register_callback_in_squad.return_value = True
        settings = self.sample_settings()
        settings.FIRMWARE = "http://firmware.url"
        self.assertTrue(test(settings))

        test_env = pathlib.Path(settings.CI_PROJECT_DIR) / "test.env"
        test_env_contents = test_env.read_text()
        self.assertIn("SQUAD_JOB_ID=1", test_env_contents)
        test_env.unlink()
