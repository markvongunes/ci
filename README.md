
| test suite / device | avadb | qemu | rockpi4b | rpi4 | stm32mp157c-dk2 | stm32mp157-ev1 | synquacer | zynqmp-starter |
| ------------------- | ----- | ---- | -------- | ---- | --------------- | -------------- | --------- | -------------- |
| build               | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=avadb-xfce&suite=build&passrate) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rockpi4b&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rpi4&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32pm157c-dk2&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-ev1&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=build&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zynqmp-starter&suite=build&hide_zeros=1) |
| xtest               | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=avadb-xfce&suite=xtest&passrate) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rockpi4b&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rpi4&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32pm157c-dk2&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-ev1&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=xtest&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zynqmp-starter&suite=xtest&hide_zeros=1) |
| secure-boot-enabled | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=avadb-xfce&suite=secure-boot-enabled&passrate) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rockpi4b&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rpi4&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32pm157c-dk2&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-ev1&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=secure-boot-enabled&hide_zeros=1) | ![badge](https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zynqmp-starter&suite=secure-boot-enabled&hide_zeros=1) |



# ARM Blueprints CI Images

This CI is intended to build and test the following projects (links point to the latest successful build for each target):
- [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/badges/master/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/commits/master) meta-ts (Trusted Substrate): https://gitlab.com/Linaro/trustedsubstrate/meta-ts
  * Download images (links point to a \*.zip file, the image is inside "images" folder):
    * [qemuarm64-secureboot](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot)
    * [rockpi4b](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b)
    * [rpi4](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rpi4.rootfs.wic.gz?job=build-meta-ts-rpi4)
    * [stm32mp157c-dk2](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-stm32mp157c-dk2.rootfs.wic.gz?job=build-meta-ts-stm32mp157c-dk2)
    * [stm32mp157c-ev1](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-stm32mp157c-ev1.rootfs.wic.gz?job=build-meta-ts-stm32mp157c-ev1)
    * [synquacer](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-synquacer.rootfs.tar.gz?job=build-meta-ts-synquacer)
    * [zynqmp-starter](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-zynqmp-starter.rootfs.tar.gz?job=build-meta-ts-zynqmp-starter)

- [![pipeline status](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/badges/kirkstone-dev/pipeline.svg)](https://gitlab.com/soafee/ewaol/meta-ewaol-machine/-/commits/main) meta-ewaol-machine: https://gitlab.com/soafee/ewaol/meta-ewaol-machine
  * Download images (links point to a \*.zip file, the image is inside "images" folder):
    * [avadb-xfce](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ewaol-baremetal-image-ava.rootfs.wic.bz2?job=build-meta-ewaol-machine-avadb-xfce)
    * [ledge-security-qemuarm64](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ewaol-baremetal-image-ledge-secure-qemuarm64.rootfs.wic.gz?job=build-meta-ewaol-machine-ledge-security-qemuarm64)

Here's a comparison table listing all targets for TS and EWAOL side-by-side:

|                 | EWAOL    | TS       |
|-----------------|----------|----------|
| avadb-xfce      | &#10004; |          |
| qemuarm64       | &#10004; | &#10004; |
| rockpi4b        |          | &#10004; |
| rpi4            |          | &#10004; |
| stm32mp157c-dk2 |          | &#10004; |
| stm32mp157c-ev1 |          | &#10004; |
| synquacer       |          | &#10004; |
| zynqmp-starter  |          | &#10004; |

# Build strategy

There will be builds:
* Everyday at 5pm UTC (scheduled pipeline in https://gitlab.com/Linaro/blueprints/nightly-builds)
* On every push in each of the components repositories listed above
  * Components repositories point to this one when running boot or test jobs that require other components, e.g. in meta-ts, when a LAVA job runs, it needs an EWAOL image, which is fetched from [nightly-builds](https://gitlab.com/Linaro/blueprints/nightly-builds) and the job that builds meta-ewaol.

## Pipelines

There will be pipelines for protected branches and merge requests coming from components repositories only.

The reasoning is that most merge requests will come from a branch created in the repository, and that will only serve as a place to host new development code.
Since there will be need to run the CI pipelines in the merge request (once opened), there is no need to run the pipelines twice.

### Merge Requests workflow

Because we are an open source project, running Gitlab's open source project, we are not allowed to have merge requests of forked repositories to trigger pipelines in parent projects.
For example, if Bob forks `meta-ts` and open a MR in `meta-ts` from Bob's fork, no pipeline is triggered. This is only available in [Gitlab's premium tier](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html#run-pipelines-in-the-parent-project). There is a hidden feature though: if Bob is a member in the parent project, Gitlab will run his pipeline on the parent project's resources.

Therefore, Blueprints/TRS teams should stick to opening merge requests using branching rather than forking.


# The builder: TuxSuite
All builds are actually run by a Linaro service called TuxSuite (tuxsuite.com). It is a newly created project initially developed to build Linux kernels at scale, abstracting all complexity related to disk space and parallel jobs. TuxSuite is in active development, and new features are added often. One of their latest added feature is the ability to re-use the same expertise learned in building kernels at scale to build OpenEmbedded projects.

One can visit their docs page (https://docs.tuxsuite.com) for further explanation of how it works. Below is an example of how we use TuxSuite to build Blueprints projects.


## How to build
Everything in TuxSuite happens in the cloud, so the only thing we need is a way to give it information about our build: that is called a "plan". Here is a working example of a plan:


```yaml
version: 1
name: "build-meta-ts-qemuarm64"
description: "Plan to Build TrustedSubstrate for Qemu"
jobs:
- name: build-meta-ts-qemuarm64
  bakes:
    - sources:
        kas:
          url: "https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git"
          yaml: "ci/qemuarm64-secureboot.yml"
      name: meta-ts-secureboot
```

Save that into `plan.yml`, then run:

```bash
$ tuxsuite plan plan.yml
```

And wait for tuxsuite's magic:

```
Running Bake plan 'build-meta-ts-qemuarm64': 'Plan to Build TrustedSubstrate for Qemu'
Plan https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV

uid: 2EAPthrX9oQnufe41igtEEm1dfV
⚙️  Provisioning: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🚀 Running: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🎉 Pass: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/

Summary: https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV
2EAPtsGSKBSa784LrKVSkFzOaWK 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
```

Yay! Now head to `https://storage.tuxsuite.com/<your-instance>` and browse through generated files.

**Note**: That actually built an whole OpenEmbedded image in the cloud, without you having to clean up your hard drive or restart the build due to connection issues. Because TuxSuite is hosted in AWS (Amazon Web Services), it is most certainly it will have a steady connection.

# How does it work?

This repository is meant to contain necessary CI scripts to be used by components elsewhere. Gitlab allows repositories point to CI scripts from external files. Therefore, there are 4 main files in this repository: `meta-ts.yml`, `meta-ewaol.yml`, `meta-ewaol-machine.yml` and `nightly-builds.yml`. Each of these are meant to be referenced within **Settings > CI/CD > General Pipelines** configuration in [linaro/trustedsubstrate/meta-ts.git](https://gitlab.com/Linaro/trustedsubstrate/meta-ts), [soafee/ewaol/meta-ewaol.git](https://gitlab.com/soafee/ewaol/meta-ewaol), [soafee/ewaol/meta-ewaol-machine.git](https://gitlab.com/soafee/ewaol/meta-ewaol-machine) and [linaro/blueprints/nightly-builds.git](https://gitlab.com/Linaro/blueprints/nightly-builds/), respectively.

```mermaid
graph TB
  sub1 --> linaro/trustedsubstrate/meta-ts.git
  sub2 --> soafee/ewaol/meta-ewaol.git
  sub3 --> soafee/ewaol/meta-ewaol-machine.git
  sub4 --> linaro/blueprints/nightly-builds.git

  subgraph "linaro/blueprints/ci"
    sub1[meta-ts.yml]
    sub2[meta-ewaol.yml]
    sub3[meta-ewaol-machine.yml]
    sub4[nightly-builds.yml]

end
```

Then Gitlab will trigger new pipelines in each respective repository.

**NOTE:** the main reason why there's a separate repository for nightly builds is that we can also run tests for CI itself. Doing both meta-ts/meta-ewaol and CI tests in the same repository became too troublesome and we decided to separate them.

## Subsystems integration

Here's an overview of how CI works along supporting subsystems (SQUAD, LAVA and TuxSuite):

```mermaid
sequenceDiagram
    participant user as Developer
    participant gitlab as Gitlab
    participant squad as SQUAD
    participant lava as LAVA
    participant tuxsuite as TuxSuite

    user->>+gitlab: git-push or MR
    gitlab->>tuxsuite: send build request
    tuxsuite-->>gitlab: build OK
    gitlab->>tuxsuite: download image
    tuxsuite-->>gitlab: image contents
    gitlab->>squad: submit build results
    squad-->>gitlab: build results submitted OK
    alt boot & test
        gitlab->>squad: submit test-job definition
        squad->>lava: submit test-job request
        lava->>gitlab: download OS and FIRMWARE
        gitlab-->>lava: OS and FIRMWARE contents
        lava-->>squad: test-job OK
	squad->>gitlab: trigger check job
        gitlab-->>squad: fetch test results
    end
```

It all starts with a `git-push` or a new merge request event. Gitlab parses the ci script for the repository and triggers a new pipeline. Gitlab then invokes `tuxsuite` cli which sends a build request to [TuxSuite](https://tuxsuite.com) service. After TuxSuite finishes building the requested image, Gitlab downloads it and store inside the job's artifact folder. Next step is to tell [SQUAD](https://qa-reports.linaro.org) that the build passed (or failed).

If there are target boards in [LAVA](https://ledge.validation.linaro.org), boot & test jobs are triggered. Gitlab generates a test-job definition, which is LAVA's specification on how to boot and test things, and use SQUAD to send it to LAVA. After LAVA downloads both OS and FIRMWARE from Gitlab, it triggers boot and tests that are specified in the test-job definition. LAVA then signals SQUAD that job is finished and results are finally fetched by SQUAD. When SQUAD finishes fetching all test results for a pipeline, it will trigger a job in Gitlab that will fetch test results from SQUAD, signaling developers that tests passed or failed.

The main reason why test-job definitions are sent to LAVA via SQUAD is so that SQUAD can collect results of all LAVA runs. And SQUAD does it nicely enough that allows users to be notified when tests fail or regress.

## How to test the BP CI changes locally

From a development standpoint, sometimes you want to test and debug changes locally before submitting a merge/pull request.

It requires to install `jinja2` and `squad-client` using `pip`.

Export `SQUAD_HOST` and `SQUAD_TOKEN` variables before run `./blueprints_ci_tests/run-self-tests.sh`

### Updating the docker image

By default, the CI docker image will be updated whenever there is a push to the Dockerfile in the main branch.
If no changes to Dockerfile are made but its internals require update, one can manually trigger a docker image
update by manually triggering a pipeline and setting a variable named `UPDATE_CI_DOCKER_IMAGE=1`.
